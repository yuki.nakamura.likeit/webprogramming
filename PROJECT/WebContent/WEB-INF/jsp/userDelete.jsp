<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title>Login Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="css/style.css" rel="stylesheet" tyep="text/css">
    </head>

    <body>

        <nav class="navbar navbar-expand-sm bg-primary navbar-dark sticky-top">
            <div class="collapse navbar-collapse justify-content-end">
                <ul class="nav navbar-nav navbar-right">
                    <li class="nav-link px-3">${sessionUser.name}さん</li>
                    <li><a class="nav-link" href="/LogoutServlet">ログアウト</a></li>
                </ul>
            </div>
        </nav>

      <div class="container">
	        <form action="UserDeleteServlet" method="post">
	            <h2 class="title my-lg-4 text-danger">ユーザ削除確認</h2>

	                <p>ログインID：${ userInfo.loginId }</p>
	                <p>を本当に削除してよろしいでしょうか。</p>
	                <input type="hidden" name="id" value="${userInfo.id}">

	          <div class="form-group">
	            <a href="UserListServlet" name="back" class="btn btn-warning text-white my-lg-4">キャンセル</a>
	            <button type="submit" name="delete" class="btn btn-danger my-lg-4">OK</button>
	          </div>
			</form>
        </div>

    </body>
</html>