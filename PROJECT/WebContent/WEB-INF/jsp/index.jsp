<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title>Login Page</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="css/style.css" rel="stylesheet" type="text/css" />
    </head>

    <body>

      <div class="container">
      	<c:if test="${errMsg != null}" >
		    <div class="alert alert-danger" role="alert">
			  ${errMsg}
			</div>
		</c:if>

      	<div class="card">
	        <form class="form" action="LoginServlet" method="post">

	            <h2 class="title my-lg-4 text-center">ログイン</h2>

	            <div class="form-group">
	              ログインID <input type="id" name="loginId" class="form-shape" id="">
	            </div>
	            <div class="form-group mb-4">
	              パスワード <input type="password" name="password" class="form-shape" id="">
	            </div>
	            <div class="form-group">
	              <button type="submit" name="login" class="btn btn-primary">ログイン</button>
	            </div>

	        </form>
	       </div>
       </div>

    </body>
</html>