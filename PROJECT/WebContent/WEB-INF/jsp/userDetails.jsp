<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title>ユーザ詳細</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="css/style.css" rel="stylesheet" tyep="text/css">
    </head>

    <body>

        <nav class="navbar navbar-expand-sm bg-primary navbar-dark sticky-top">
            <div class="collapse navbar-collapse justify-content-end">
                <ul class="nav navbar-nav navbar-right">
                    <li class="nav-link px-3">${sessionUser.name}さん</li>
                    <li><a class="nav-link" href="LogoutServlet">ログアウト</a></li>
                </ul>
            </div>
        </nav>

      <div class="container">
        <form>
            <h2 class="title my-lg-4">ユーザ情報詳細参照</h2>

                    <div class="row">

                        <div class="col-6">
                            <label for="">ログインID</label>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                ${userInfo.loginId}
                            </div>
                        </div>

                        <div class="col-6">
                            <label for="">ユーザ名</label>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                ${userInfo.name}
                            </div>
                        </div>

                        <div class="col-6">
                            <label for="">生年月日</label>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                ${userInfo.birthDate}
                            </div>
                        </div>

                        <div class="col-6">
                            <label for="">登録日時</label>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                ${userInfo.createDate}
                            </div>
                        </div>

                        <div class="col-6">
                            <label for="">更新日時</label>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                ${userInfo.updateDate}
                            </div>
                        </div>


                    </div>

          </form>

          <div class="form-group">
            <a href="UserListServlet" name="back" class="btn btn-warning text-white my-lg-4">戻る</a>
          </div>

        </div>

    </body>
</html>