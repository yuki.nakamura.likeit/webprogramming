<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title>ユーザ登録</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="css/style.css" rel="stylesheet" tyep="text/css">
    </head>

    <body>

        <nav class="navbar navbar-expand-sm bg-primary navbar-dark sticky-top">
            <div class="collapse navbar-collapse justify-content-end">
                <ul class="nav navbar-nav navbar-right">
                    <li class="nav-link px-3">${sessionUser.name}さん</li>
                    <li><a class="nav-link" href="LogoutServlet">ログアウト</a></li>
                </ul>
            </div>
        </nav>

            <div class="container">

                <form action="UserRegisterServlet" method="post">
                    <h2 class="title my-lg-4">ユーザ新規登録</h2>
                    	<c:if test="${errMsg != null}" >
						    <div class="alert alert-danger" role="alert">
							  ${errMsg}
							</div>
						</c:if>

                    <div class="row">

                        <div class="col-6">
                                <label for="">ログインID</label>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <input type="text" name="loginId" class="form-control" value="${loginId}">
                            </div>
                        </div>

                        <div class="col-6">
                            <label for="">パスワード</label>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <input type="password" name="password" class="form-control">
                            </div>
                        </div>

                        <div class="col-6">
                            <label for="">パスワード(確認)</label>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <input type="password" name="confirmPassword" class="form-control">
                            </div>
                        </div>

                        <div class="col-6">
                            <label for="">ユーザー名</label>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <input type="text" name="name" class="form-control" value="${name}">
                            </div>
                        </div>

                        <div class="col-6">
                            <label for="">生年月日</label>
                        </div>
                        <div class="col-4">
                            <div class="form-group">
                                <input type="date" name="birthDate" class="form-control" value="${birthDate}">
                            </div>
                        </div>

						<div class="col-12">
	                        <div class="form-group pt-5">
			                    <a href="UserListServlet" name="back" class="btn btn-warning text-white">戻る</a>
			                    <button type="submit" name="register" class="btn btn-primary">登録</button>
			                </div>
			              </div>

                    </div>
                </form>



            </div>

    </body>
</html>