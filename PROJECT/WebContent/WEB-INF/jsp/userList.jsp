<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>

    <head>
        <meta charset="UTF-8">
        <title>ユーザリスト</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="css/style.css" rel="stylesheet" tyep="text/css">
    </head>

    <body>

        <nav class="navbar navbar-expand-sm bg-primary navbar-dark sticky-top">
            <div class="collapse navbar-collapse justify-content-end">
                <ul class="nav navbar-nav navbar-right">
                    <li class="nav-link px-3">${sessionUser.name}さん</li>
                    <li><a class="nav-link" href="LogoutServlet">ログアウト</a></li>
                </ul>
            </div>
        </nav>

      <div class="container">
      	<c:if test="${errMsg != null}" >
		    <div class="alert alert-danger" role="alert">
			  ${errMsg}
			</div>
		</c:if>

        <form action="UserListServlet" method="post">

            <h2 class="title my-lg-4">ユーザ一覧</h2>

            <a class="float-right" href="UserRegisterServlet">新規登録</a>
            <div class="form-group">
              ログインID <input type="text" name="loginId" class="form-shape" id="">
            </div>
            <div class="form-group mb-4">
              ユーザ名 <input type="text" name="name" class="form-shape" id="">
            </div>
            <div class="form-group mb-4">
              生年月日 <input type="date" name="startDate" class="form-shape" id="">
              ~
              <input type="date" name="endDate" class="form-shape" id="">
            </div>
            <div class="form-group">
              <button type="submit" name="search" class="btn btn-primary">検索</button>
            </div>

          </form>
        </div>

        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <table class="table table-striped">
                        <thead class="thead-dark">
                            <th>ログインID</th>
                            <th>ユーザ名</th>
                            <th>生年月日</th>
                            <th></th>
                        </thead>

                        <tbody>
                            <c:forEach var="user" items="${userList}" >
			                   <tr>
			                     <td>${user.loginId}</td>
			                     <td>${user.name}</td>
			                     <td>${user.birthDate}</td>
			                     <c:choose>

			                     	<c:when test="${sessionUser.loginId == 'admin'}">
				                     <td>
				                       <a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
				                       <a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
				                       <a class="btn btn-danger" href ="UserDeleteServlet?id=${user.id}">削除</a>
				                     </td>
				                   	</c:when>

				                 	<c:when test="${sessionUser.loginId != 'admin'}">
				                	 <td>
				                       	<a class="btn btn-primary" href="UserDetailServlet?id=${user.id}">詳細</a>
									<c:if test="${sessionUser.loginId == user.loginId}">
				                       	<a class="btn btn-success" href="UserUpdateServlet?id=${user.id}">更新</a>
				                    </c:if>
				                     </td>
				                   </c:when>

				                 </c:choose>
			                   </tr>
			                 </c:forEach>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </body>
</html>