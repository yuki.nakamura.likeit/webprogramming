package controller;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.UserDao;
import model.User;

/**
 * Servlet implementation class UserUpdateServlet
 */
@WebServlet("/UserUpdateServlet")
public class UserUpdateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

    /**
     * @see HttpServlet#HttpServlet()
     */
    public UserUpdateServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		int selectedId = Integer.parseInt(request.getParameter("id"));

		UserDao userDao = new UserDao();
		User user = userDao.findUser(selectedId);

		request.setAttribute("userInfo", user);

		RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
		dispatcher.forward(request, response);
	}





	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("UTF-8");

		int selectedId = Integer.parseInt(request.getParameter("id"));
		String password = request.getParameter("password");
		String confirmPassword = request.getParameter("confirmPassword");
		String name = request.getParameter("name");
		String birthDate = request.getParameter("birthDate");

		UserDao userDao = new UserDao();
		User user = userDao.findUser(selectedId);

		//パスワードが合致しない
		if(!(password.equals(confirmPassword))) {
			request.setAttribute("errMsg", "パスワードが合致しません");

			//入力欄に文字を残しておく
			request.setAttribute("userInfo", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;
		}

		//名前と誕生日が未入力の場合
		if(name.equals("") || birthDate.equals("")) {
			request.setAttribute("errMsg", "未入力の項目があります");

			//入力欄に文字を残しておく
			request.setAttribute("userInfo", user);

			RequestDispatcher dispatcher = request.getRequestDispatcher("/WEB-INF/jsp/userUpdate.jsp");
			dispatcher.forward(request, response);
			return;

		}

		//パスワード変更あり
		if(!(password.equals("")) && !(confirmPassword.equals(""))){
				userDao.updateUser(selectedId, password, name, birthDate);
		} else {
		//パスワード変更なし
			userDao.updateUserWithoutPass(selectedId, name, birthDate);
		}

			//成功した場合
			response.sendRedirect("UserListServlet");
		}
}
