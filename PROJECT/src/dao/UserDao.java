package dao;


import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.DatatypeConverter;

import model.User;

/**
 * ユーザテーブル用のDao
 * @author takano
 *
 */
public class UserDao {

    /**
     * ログインIDとパスワードに紐づくユーザ情報を返す
     * @param loginId
     * @param password
     * @return
     */
    public User findByLoginInfo(String loginId, String password) {
        Connection conn = null;
        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id = ? and password = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, hashGenerator(password));
            ResultSet rs = pStmt.executeQuery();


            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            String nameData = rs.getString("name");
            return new User(loginIdData, nameData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }


    /**
     * 全てのユーザ情報を取得する
     * @return
     */
    public List<User> findAll() {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT * FROM user WHERE login_id != 'admin'";

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
                int id = rs.getInt("id");
                String loginId = rs.getString("login_id");
                String name = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginId, name, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }

    //全検索
    public List<User> search(String loginId, String name, String startDate, String endDate) {
        Connection conn = null;
        List<User> userList = new ArrayList<User>();

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            String sql = "SELECT * FROM user WHERE login_id != 'admin'";

         // ログインIDの入力値が空じゃなかったら
            if(!loginId.equals("")) {
              sql += " AND  login_id = '" + loginId + "'";
            }

            // 名前の入力値が空じゃなかったら
            if(!name.equals("")){
            	sql += "AND name LIKE'%" + name + "%'";
            }

            // 生年月日（始まり）の入力値が空じゃなかったら
            if(!startDate.equals("")){
            	sql += "AND birth_date >= '" + startDate + "'";
            }

            // 生年月日（終わり）名前の入力値が空じゃなかったら
            if(!endDate.equals("")){
            	sql += "AND birth_date <= '" + endDate + "'";
            }

             // SELECTを実行し、結果表を取得
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            // 結果表に格納されたレコードの内容を
            // Userインスタンスに設定し、ArrayListインスタンスに追加
            while (rs.next()) {
            	int id = rs.getInt("id");
                String loginIdData = rs.getString("login_id");
                String nameData = rs.getString("name");
                Date birthDate = rs.getDate("birth_date");
                String password = rs.getString("password");
                String createDate = rs.getString("create_date");
                String updateDate = rs.getString("update_date");
                User user = new User(id, loginIdData, nameData, birthDate, password, createDate, updateDate);

                userList.add(user);
            }
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
        return userList;
    }

    //ユーザ名のみ検索
//    public List<User> findByName(String name) {
//    	Connection conn = null;
//    	List<User> userList = new ArrayList<User>();
//
//        try {
//        	conn = DBManager.getConnection();
//
//            String sql = "SELECT * FROM user WHERE name LIKE ?";
//
//            PreparedStatement pStmt = conn.prepareStatement(sql);
//            pStmt.setString(1, "%" + name + "%");
//            ResultSet rs = pStmt.executeQuery();
//
//            while (rs.next()) {
//                int idData = rs.getInt("id");
//                String loginIdData = rs.getString("login_id");
//                String nameData = rs.getString("name");
//                Date birthDateData = rs.getDate("birth_date");
//                String passwordData = rs.getString("password");
//                String createDateData = rs.getString("create_date");
//                String updateDateData = rs.getString("update_date");
//                User user = new User(idData, loginIdData, nameData, birthDateData, passwordData, createDateData, updateDateData);
//
//                userList.add(user);
//            }
//        } catch (SQLException e) {
//            e.printStackTrace();
//            return null;
//        } finally {
//            // データベース切断
//            if (conn != null) {
//                try {
//                    conn.close();
//                } catch (SQLException e) {
//                    e.printStackTrace();
//                    return null;
//                }
//            }
//        }
//        return userList;
//    }

    //ユーザ詳細
    public User findUser(int selectedId) {
        Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            String sql = "SELECT * FROM user WHERE id = " + selectedId;

            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            if (!rs.next()) {
                return null;
            }

            String loginId = rs.getString("login_id");
            String name = rs.getString("name");
            Date birthDate = rs.getDate("birth_date");
            String password = rs.getString("password");
            String createDate = rs.getString("create_date");
            String updateDate = rs.getString("update_date");

            return new User(selectedId, loginId, name, birthDate, password, createDate, updateDate);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }
    }

    //ユーザ更新(パスワードあり)
    public void updateUser(int selectedId, String password, String name, String birthDate) {
        Connection conn = null;

        try {
            conn = DBManager.getConnection();

            String sql = "UPDATE user SET name = ?, birth_date = ?, password = ?, update_date = NOW() WHERE id = ?";

            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, name);
            pStmt.setString(2, birthDate);
            pStmt.setString(3, hashGenerator(password));
            pStmt.setInt(4, selectedId);
            int rs = pStmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //ユーザ更新(パスワードなし)
    public void updateUserWithoutPass(int selectedId, String name, String birthDate) {
        Connection conn = null;

        try {
            conn = DBManager.getConnection();

            String sql = "UPDATE user SET name = ?, birth_date = ?, update_date = NOW() WHERE id = ?";

            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, name);
            pStmt.setString(2, birthDate);
            pStmt.setInt(3, selectedId);
            int rs = pStmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }


    //ユーザ削除
    public void deleteUser(String id) {
        Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            String sql = "DELETE FROM user WHERE id = " + id;

            Statement stmt = conn.createStatement();
            int rs = stmt.executeUpdate(sql);

        } catch (SQLException e) {
            e.printStackTrace();

        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //ユーザ新規登録
    public void registerUser(String loginId, String password,  String name, String birthDate) {
        Connection conn = null;

        try {
            conn = DBManager.getConnection();

            String sql = "INSERT INTO user(login_id, name, birth_date, password, create_date, update_date) VALUES(?, ?, ?, ?, NOW(), NOW())";

            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            pStmt.setString(2, name);
            pStmt.setString(3, birthDate);
            pStmt.setString(4, hashGenerator(password));
            int rs = pStmt.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    //ユーザー新規登録で既にログインIDがないか確認
    public User searchLoginId(String loginId) {
    	Connection conn = null;

        try {
            // データベースへ接続
            conn = DBManager.getConnection();

            // SELECT文を準備
            String sql = "SELECT login_id FROM user WHERE login_id = ?";

             // SELECTを実行し、結果表を取得
            PreparedStatement pStmt = conn.prepareStatement(sql);
            pStmt.setString(1, loginId);
            ResultSet rs = pStmt.executeQuery();

            // 主キーに紐づくレコードは1件のみなので、rs.next()は1回だけ行う
            if (!rs.next()) {
                return null;
            }

            // 必要なデータのみインスタンスのフィールドに追加
            String loginIdData = rs.getString("login_id");
            return new User(loginIdData);

        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        } finally {
            // データベース切断
            if (conn != null) {
                try {
                    conn.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                    return null;
                }
            }
        }

    }

    //パスワード暗号化
    public String hashGenerator(String password) {

	    	String source = password;
	    	Charset charset = StandardCharsets.UTF_8;
	    	String algorithm = "MD5";

	    	byte[] bytes;
			try {
				bytes = MessageDigest.getInstance(algorithm).digest(source.getBytes(charset));
			} catch (NoSuchAlgorithmException e) {
				e.printStackTrace();
				return null;
			}

	    	//sourceに代入した値を暗号化したものが変数resultに入る
	    	String result = DatatypeConverter.printHexBinary(bytes);
	    	return result;
    }

} //end of UserDao

